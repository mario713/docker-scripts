#/bin/sh

echo "UPLOAD_PATH = $UPLOAD_PATH" > /app/.env
echo "PROTOCOL_TYPE = $PROTOCOL_TYPE" >> /app/.env
echo "DB_USER = $DB_USER" >> /app/.env
echo "DB_PASS = $DB_PASS" >> /app/.env
echo "DB_NAME = $DB_NAME" >> /app/.env


cd /app && nodemon

tailf -f /dev/null